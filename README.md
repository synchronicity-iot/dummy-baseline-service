## Description

***Very important, during all the readme think that is going to be readed by someone outside the project. They dont need to know about Synchronicity deliverables, milestons, application themes, RZ,s etc. Think on a developer willing to use a service that is build over a Synchronicity Framework and their minimum interoperability points (Historical API, context management API, Data Marketplace?). A developer reaching here has a minimum knoweldge about Synchronicity Arquitecture***

This baseline service is offering the means to aggregate and compute data for the purpose of....

Check [this link](https://synchronicity-iot.eu/tech/atomic-services/) in order to get the summary of what it supposed to be in this part.

* What is this atomic service
* Challenge addressed
* Picture (e.g. snapshot of a GUI)

PS: Feel free to copy the text and amend it.

***Very imporant, remember the principles of an Atomic Service: foucs on the re-usability and city agnostic:***
 * Synchronicity framework interoperability points used by the atomic services (Historical API, context management API, Data Marketplace?)
 * Input data models (NGSI Datamodels)
 * Output data models (NGSI Datamodels)

 


## How to use it

If your service is mora a kind of application, describe how to use it.


## Interface and API


The API is based on the OMA NGSI. The data format is based on JSON. 
The service acts synchronously and each request will be answered with a single response.
For further information please refer to the APIary link (if any)

## Service Architecture


This baseline services is composed by X atomic blocks: block A that does ...; block B that does ...
Block A interacts with block B...
For more detailed information of the service specification please refer to the link...

## Service Deployment


Here will go a quick installation and admin guide for having the service up and running trough docker.
A link to more comprehensive documentation about configurations and features might be provided.

Explain how to download, install, configure, etc. Mandtory via docker, and optional other alternatives.


### Via Docker

Point to the docker page or put here docker instructions

## (optional) Native installation
In case there are instructions on how to install directly on a machine without docker


## System Requirements

Minimum requirements to run this atomic service:

* Ubuntu 16.04
* CPU Architecture (x86_64)
* CouchDB 1.6.0


## (optional) About

More information at this link...

## Support


The support for this baseline services can be request via the issue tracker [here](https://github.com/telefonicaid/fiware-orion/issues)


## License & Terms and Conditions


This baseline service is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version. Please refer to [gnu licenses](https://www.gnu.org/licenses/) for more information.

The software is released as it is and we discharge any liability.